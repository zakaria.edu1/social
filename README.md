### Demo link

- [Demo](https://social45.herokuapp.com/)

### Ruby version

- ruby 2.5.1

- Rails 5.2.1

### Gems

- [Better Errors](https://rubygems.org/gems/better_errors) - Make errors prettier
- [Bulma](https://github.com/joshuajansen/bulma-rails) - Bulma CSS
- [Simple Form](https://github.com/plataformatec/simple_form) - Simple forms
- [Guard](https://github.com/guard/guard) - Guard is a command line tool to easily handle events on file system modifications
- [Guard LiveReload](https://github.com/guard/guard-livereload) - reload the browser after changes to assets/helpers/tests
- [Devise](https://github.com/plataformatec/devise) - Flexible authentication solution for Rails with Warden

### To-do

- tambah table user kolom username
- post.user ==  user.username
- friend
- post only related to user/friend
