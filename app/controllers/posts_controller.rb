class PostsController < ApplicationController
  before_action :find_post, only: [:show, :update, :destroy]
  before_action :authenticate_user!, except: [:show, :index]
  def index
    @post = Post.new
    @posts = Post.all.order("created_at DESC")
  end

  def create
    @post = Post.new(post_params)

    if @post.save
      redirect_to request.referrer
    else
      render 'index'
    end
  end

  def show
  end

  def update

    if @post.update(post_params)
      redirect_to @post
    else
      render 'index'
    end
  end

  def edit
    @post = Post.find(params[:id])
  end

  def destroy
    @post.destroy

    redirect_to posts_path
  end

  private

  def post_params
    params.require(:post).permit(:user, :post)
  end

  def find_post
    @post = Post.find(params[:id])
  end

end
